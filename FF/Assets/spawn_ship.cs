﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_ship : MonoBehaviour {
	public float secondsBetweenSpawning = 0.1f;
	public float xMinRange = -25.0f;
	public float xMaxRange = 25.0f;
	public float yMinRange = 8.0f;
	public float yMaxRange = 25.0f;
	public float zMinRange = -25.0f;
	public float zMaxRange = 25.0f;
	public GameObject spawnObjects; // what prefabs to spawn
	int max = 5,count =0;
	private float nextSpawnTime;

	// Use this for initialization
	void Start () {
		nextSpawnTime = Time.time+secondsBetweenSpawning;
	}
	
	// Update is called once per frame
	void Update () {


		if (Time.time  >= nextSpawnTime && count<max) {
			// Spawn the game object through function below
			count++;
			MakeThingToSpawn ();

			// determine the next time to spawn the object
			nextSpawnTime = Time.time+secondsBetweenSpawning;
		}

	}

	void MakeThingToSpawn ()
	{
		Vector3 spawnPosition;

		// get a random position between the specified ranges
		spawnPosition.x = Random.Range (xMinRange, xMaxRange);
		spawnPosition.y = Random.Range (yMinRange, yMaxRange);
		spawnPosition.z = Random.Range (zMinRange, zMaxRange);

		// determine which object to spawn
		//int objectToSpawn = Random.Range (0, 3);

		// actually spawn the game object
		GameObject spawnedObject = Instantiate (spawnObjects, spawnPosition, transform.rotation) as GameObject;

		// make the parent the spawner so hierarchy doesn't get super messy
		spawnedObject.transform.parent = gameObject.transform;
	}
}
