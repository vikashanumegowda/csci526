﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JoystickLooker
struct JoystickLooker_t1951869554;

#include "codegen/il2cpp-codegen.h"

// System.Void JoystickLooker::.ctor()
extern "C"  void JoystickLooker__ctor_m1119335649 (JoystickLooker_t1951869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoystickLooker::Start()
extern "C"  void JoystickLooker_Start_m1378765489 (JoystickLooker_t1951869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoystickLooker::Update()
extern "C"  void JoystickLooker_Update_m2970283440 (JoystickLooker_t1951869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoystickLooker::LookRotation()
extern "C"  void JoystickLooker_LookRotation_m898385688 (JoystickLooker_t1951869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
