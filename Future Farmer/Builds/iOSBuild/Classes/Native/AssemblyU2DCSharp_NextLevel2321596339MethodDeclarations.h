﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NextLevel
struct NextLevel_t2321596339;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void NextLevel::.ctor()
extern "C"  void NextLevel__ctor_m2680661740 (NextLevel_t2321596339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NextLevel::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void NextLevel_OnCollisionEnter_m1872425830 (NextLevel_t2321596339 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
