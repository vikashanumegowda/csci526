﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.CharacterController
struct CharacterController_t4094781467;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t1937198888  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Controller::moveSpeed
	float ___moveSpeed_2;
	// System.Single Controller::jumpSpeed
	float ___jumpSpeed_3;
	// System.Single Controller::gravity
	float ___gravity_4;
	// UnityEngine.CharacterController Controller::myController
	CharacterController_t4094781467 * ___myController_5;
	// UnityEngine.Vector3 Controller::moveDirection
	Vector3_t2243707580  ___moveDirection_6;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_3() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___jumpSpeed_3)); }
	inline float get_jumpSpeed_3() const { return ___jumpSpeed_3; }
	inline float* get_address_of_jumpSpeed_3() { return &___jumpSpeed_3; }
	inline void set_jumpSpeed_3(float value)
	{
		___jumpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_gravity_4() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___gravity_4)); }
	inline float get_gravity_4() const { return ___gravity_4; }
	inline float* get_address_of_gravity_4() { return &___gravity_4; }
	inline void set_gravity_4(float value)
	{
		___gravity_4 = value;
	}

	inline static int32_t get_offset_of_myController_5() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___myController_5)); }
	inline CharacterController_t4094781467 * get_myController_5() const { return ___myController_5; }
	inline CharacterController_t4094781467 ** get_address_of_myController_5() { return &___myController_5; }
	inline void set_myController_5(CharacterController_t4094781467 * value)
	{
		___myController_5 = value;
		Il2CppCodeGenWriteBarrier(&___myController_5, value);
	}

	inline static int32_t get_offset_of_moveDirection_6() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___moveDirection_6)); }
	inline Vector3_t2243707580  get_moveDirection_6() const { return ___moveDirection_6; }
	inline Vector3_t2243707580 * get_address_of_moveDirection_6() { return &___moveDirection_6; }
	inline void set_moveDirection_6(Vector3_t2243707580  value)
	{
		___moveDirection_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
