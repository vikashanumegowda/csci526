﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Rotate
struct Rotate_t4255939431;

#include "codegen/il2cpp-codegen.h"

// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1487335446 (Rotate_t4255939431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Update()
extern "C"  void Rotate_Update_m4217433871 (Rotate_t4255939431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
