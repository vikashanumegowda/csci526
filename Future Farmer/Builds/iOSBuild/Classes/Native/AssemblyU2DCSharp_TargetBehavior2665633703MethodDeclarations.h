﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetBehavior
struct TargetBehavior_t2665633703;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void TargetBehavior::.ctor()
extern "C"  void TargetBehavior__ctor_m3571505626 (TargetBehavior_t2665633703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetBehavior::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void TargetBehavior_OnCollisionEnter_m860962568 (TargetBehavior_t2665633703 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
