﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedObjectDestructor
struct TimedObjectDestructor_t3793803729;

#include "codegen/il2cpp-codegen.h"

// System.Void TimedObjectDestructor::.ctor()
extern "C"  void TimedObjectDestructor__ctor_m1281788410 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedObjectDestructor::Awake()
extern "C"  void TimedObjectDestructor_Awake_m3826250289 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedObjectDestructor::DestroyNow()
extern "C"  void TimedObjectDestructor_DestroyNow_m2242008382 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
