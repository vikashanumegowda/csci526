﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicTargetMover
struct  BasicTargetMover_t538935388  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BasicTargetMover::doSpin
	bool ___doSpin_2;
	// System.Boolean BasicTargetMover::doMotion
	bool ___doMotion_3;
	// System.Single BasicTargetMover::spinSpeed
	float ___spinSpeed_4;
	// System.Single BasicTargetMover::motionMagnitude
	float ___motionMagnitude_5;

public:
	inline static int32_t get_offset_of_doSpin_2() { return static_cast<int32_t>(offsetof(BasicTargetMover_t538935388, ___doSpin_2)); }
	inline bool get_doSpin_2() const { return ___doSpin_2; }
	inline bool* get_address_of_doSpin_2() { return &___doSpin_2; }
	inline void set_doSpin_2(bool value)
	{
		___doSpin_2 = value;
	}

	inline static int32_t get_offset_of_doMotion_3() { return static_cast<int32_t>(offsetof(BasicTargetMover_t538935388, ___doMotion_3)); }
	inline bool get_doMotion_3() const { return ___doMotion_3; }
	inline bool* get_address_of_doMotion_3() { return &___doMotion_3; }
	inline void set_doMotion_3(bool value)
	{
		___doMotion_3 = value;
	}

	inline static int32_t get_offset_of_spinSpeed_4() { return static_cast<int32_t>(offsetof(BasicTargetMover_t538935388, ___spinSpeed_4)); }
	inline float get_spinSpeed_4() const { return ___spinSpeed_4; }
	inline float* get_address_of_spinSpeed_4() { return &___spinSpeed_4; }
	inline void set_spinSpeed_4(float value)
	{
		___spinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_motionMagnitude_5() { return static_cast<int32_t>(offsetof(BasicTargetMover_t538935388, ___motionMagnitude_5)); }
	inline float get_motionMagnitude_5() const { return ___motionMagnitude_5; }
	inline float* get_address_of_motionMagnitude_5() { return &___motionMagnitude_5; }
	inline void set_motionMagnitude_5(float value)
	{
		___motionMagnitude_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
