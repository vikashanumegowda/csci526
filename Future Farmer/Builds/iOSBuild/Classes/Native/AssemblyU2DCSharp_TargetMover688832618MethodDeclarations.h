﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMover
struct TargetMover_t688832618;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetMover::.ctor()
extern "C"  void TargetMover__ctor_m1600658069 (TargetMover_t688832618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMover::Update()
extern "C"  void TargetMover_Update_m1970153192 (TargetMover_t688832618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
