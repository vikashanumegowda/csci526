﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager
struct GameManager_t2252321495;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager::get_hasGameStarted()
extern "C"  bool GameManager_get_hasGameStarted_m1794466706 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::set_hasGameStarted(System.Boolean)
extern "C"  void GameManager_set_hasGameStarted_m2949618165 (GameManager_t2252321495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Start()
extern "C"  void GameManager_Start_m2655388892 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Update()
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::EndGame()
extern "C"  void GameManager_EndGame_m2742763215 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::BeatLevel()
extern "C"  void GameManager_BeatLevel_m1637775698 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::targetHit(System.Int32,System.Single)
extern "C"  void GameManager_targetHit_m589975860 (GameManager_t2252321495 * __this, int32_t ___scoreAmount0, float ___timeAmount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::RestartGame()
extern "C"  void GameManager_RestartGame_m1828089451 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::NextLevel()
extern "C"  void GameManager_NextLevel_m1326141459 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::StartGame()
extern "C"  void GameManager_StartGame_m4019248290 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::LoadPreviousLevel()
extern "C"  void GameManager_LoadPreviousLevel_m569945163 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
