﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickLooker
struct  JoystickLooker_t1951869554  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Quaternion JoystickLooker::m_CharacterTargetRot
	Quaternion_t4030073918  ___m_CharacterTargetRot_2;
	// UnityEngine.Quaternion JoystickLooker::m_CameraTargetRot
	Quaternion_t4030073918  ___m_CameraTargetRot_3;
	// UnityEngine.Transform JoystickLooker::character
	Transform_t3275118058 * ___character_4;
	// UnityEngine.Transform JoystickLooker::cameraTransform
	Transform_t3275118058 * ___cameraTransform_5;
	// UnityEngine.GameObject JoystickLooker::m_joystick
	GameObject_t1756533147 * ___m_joystick_6;

public:
	inline static int32_t get_offset_of_m_CharacterTargetRot_2() { return static_cast<int32_t>(offsetof(JoystickLooker_t1951869554, ___m_CharacterTargetRot_2)); }
	inline Quaternion_t4030073918  get_m_CharacterTargetRot_2() const { return ___m_CharacterTargetRot_2; }
	inline Quaternion_t4030073918 * get_address_of_m_CharacterTargetRot_2() { return &___m_CharacterTargetRot_2; }
	inline void set_m_CharacterTargetRot_2(Quaternion_t4030073918  value)
	{
		___m_CharacterTargetRot_2 = value;
	}

	inline static int32_t get_offset_of_m_CameraTargetRot_3() { return static_cast<int32_t>(offsetof(JoystickLooker_t1951869554, ___m_CameraTargetRot_3)); }
	inline Quaternion_t4030073918  get_m_CameraTargetRot_3() const { return ___m_CameraTargetRot_3; }
	inline Quaternion_t4030073918 * get_address_of_m_CameraTargetRot_3() { return &___m_CameraTargetRot_3; }
	inline void set_m_CameraTargetRot_3(Quaternion_t4030073918  value)
	{
		___m_CameraTargetRot_3 = value;
	}

	inline static int32_t get_offset_of_character_4() { return static_cast<int32_t>(offsetof(JoystickLooker_t1951869554, ___character_4)); }
	inline Transform_t3275118058 * get_character_4() const { return ___character_4; }
	inline Transform_t3275118058 ** get_address_of_character_4() { return &___character_4; }
	inline void set_character_4(Transform_t3275118058 * value)
	{
		___character_4 = value;
		Il2CppCodeGenWriteBarrier(&___character_4, value);
	}

	inline static int32_t get_offset_of_cameraTransform_5() { return static_cast<int32_t>(offsetof(JoystickLooker_t1951869554, ___cameraTransform_5)); }
	inline Transform_t3275118058 * get_cameraTransform_5() const { return ___cameraTransform_5; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_5() { return &___cameraTransform_5; }
	inline void set_cameraTransform_5(Transform_t3275118058 * value)
	{
		___cameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___cameraTransform_5, value);
	}

	inline static int32_t get_offset_of_m_joystick_6() { return static_cast<int32_t>(offsetof(JoystickLooker_t1951869554, ___m_joystick_6)); }
	inline GameObject_t1756533147 * get_m_joystick_6() const { return ___m_joystick_6; }
	inline GameObject_t1756533147 ** get_address_of_m_joystick_6() { return &___m_joystick_6; }
	inline void set_m_joystick_6(GameObject_t1756533147 * value)
	{
		___m_joystick_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_joystick_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
