﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayAgain
struct PlayAgain_t1655768986;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void PlayAgain::.ctor()
extern "C"  void PlayAgain__ctor_m1254445871 (PlayAgain_t1655768986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayAgain::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void PlayAgain_OnCollisionEnter_m3365844533 (PlayAgain_t1655768986 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
