﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shooter
struct Shooter_t3051933708;

#include "codegen/il2cpp-codegen.h"

// System.Void Shooter::.ctor()
extern "C"  void Shooter__ctor_m2111854087 (Shooter_t3051933708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shooter::Update()
extern "C"  void Shooter_Update_m3198244586 (Shooter_t3051933708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
