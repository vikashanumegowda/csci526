﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasicTargetMover
struct BasicTargetMover_t538935388;

#include "codegen/il2cpp-codegen.h"

// System.Void BasicTargetMover::.ctor()
extern "C"  void BasicTargetMover__ctor_m3458125707 (BasicTargetMover_t538935388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasicTargetMover::Update()
extern "C"  void BasicTargetMover_Update_m4246744410 (BasicTargetMover_t538935388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
