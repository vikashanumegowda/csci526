﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller
struct Controller_t1937198888;

#include "codegen/il2cpp-codegen.h"

// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Start()
extern "C"  void Controller_Start_m165415507 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Update()
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
