﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameManager
struct GameManager_t2252321495;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameManager::player
	GameObject_t1756533147 * ___player_3;
	// System.Int32 GameManager::score
	int32_t ___score_4;
	// System.Boolean GameManager::canBeatLevel
	bool ___canBeatLevel_5;
	// System.Int32 GameManager::beatLevelScore
	int32_t ___beatLevelScore_6;
	// System.Single GameManager::startTime
	float ___startTime_7;
	// UnityEngine.UI.Text GameManager::mainScoreDisplay
	Text_t356221433 * ___mainScoreDisplay_8;
	// UnityEngine.UI.Text GameManager::mainTimerDisplay
	Text_t356221433 * ___mainTimerDisplay_9;
	// UnityEngine.UI.Text GameManager::restartMessageDisplay
	Text_t356221433 * ___restartMessageDisplay_10;
	// UnityEngine.GameObject GameManager::gameOverScoreOutline
	GameObject_t1756533147 * ___gameOverScoreOutline_11;
	// UnityEngine.AudioSource GameManager::musicAudioSource
	AudioSource_t1135106623 * ___musicAudioSource_12;
	// System.Boolean GameManager::gameIsOver
	bool ___gameIsOver_13;
	// UnityEngine.GameObject GameManager::playAgainButtons
	GameObject_t1756533147 * ___playAgainButtons_14;
	// System.String GameManager::playAgainLevelToLoad
	String_t* ___playAgainLevelToLoad_15;
	// UnityEngine.GameObject GameManager::nextLevelButtons
	GameObject_t1756533147 * ___nextLevelButtons_16;
	// System.String GameManager::nextLevelToLoad
	String_t* ___nextLevelToLoad_17;
	// System.String GameManager::previousLevelToLoad
	String_t* ___previousLevelToLoad_18;
	// UnityEngine.GameObject GameManager::gameStartCanvas
	GameObject_t1756533147 * ___gameStartCanvas_19;
	// UnityEngine.GameObject GameManager::mainCanvas
	GameObject_t1756533147 * ___mainCanvas_20;
	// System.Single GameManager::currentTime
	float ___currentTime_21;
	// System.Boolean GameManager::<hasGameStarted>k__BackingField
	bool ___U3ChasGameStartedU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier(&___player_3, value);
	}

	inline static int32_t get_offset_of_score_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___score_4)); }
	inline int32_t get_score_4() const { return ___score_4; }
	inline int32_t* get_address_of_score_4() { return &___score_4; }
	inline void set_score_4(int32_t value)
	{
		___score_4 = value;
	}

	inline static int32_t get_offset_of_canBeatLevel_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___canBeatLevel_5)); }
	inline bool get_canBeatLevel_5() const { return ___canBeatLevel_5; }
	inline bool* get_address_of_canBeatLevel_5() { return &___canBeatLevel_5; }
	inline void set_canBeatLevel_5(bool value)
	{
		___canBeatLevel_5 = value;
	}

	inline static int32_t get_offset_of_beatLevelScore_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___beatLevelScore_6)); }
	inline int32_t get_beatLevelScore_6() const { return ___beatLevelScore_6; }
	inline int32_t* get_address_of_beatLevelScore_6() { return &___beatLevelScore_6; }
	inline void set_beatLevelScore_6(int32_t value)
	{
		___beatLevelScore_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}

	inline static int32_t get_offset_of_mainScoreDisplay_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___mainScoreDisplay_8)); }
	inline Text_t356221433 * get_mainScoreDisplay_8() const { return ___mainScoreDisplay_8; }
	inline Text_t356221433 ** get_address_of_mainScoreDisplay_8() { return &___mainScoreDisplay_8; }
	inline void set_mainScoreDisplay_8(Text_t356221433 * value)
	{
		___mainScoreDisplay_8 = value;
		Il2CppCodeGenWriteBarrier(&___mainScoreDisplay_8, value);
	}

	inline static int32_t get_offset_of_mainTimerDisplay_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___mainTimerDisplay_9)); }
	inline Text_t356221433 * get_mainTimerDisplay_9() const { return ___mainTimerDisplay_9; }
	inline Text_t356221433 ** get_address_of_mainTimerDisplay_9() { return &___mainTimerDisplay_9; }
	inline void set_mainTimerDisplay_9(Text_t356221433 * value)
	{
		___mainTimerDisplay_9 = value;
		Il2CppCodeGenWriteBarrier(&___mainTimerDisplay_9, value);
	}

	inline static int32_t get_offset_of_restartMessageDisplay_10() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___restartMessageDisplay_10)); }
	inline Text_t356221433 * get_restartMessageDisplay_10() const { return ___restartMessageDisplay_10; }
	inline Text_t356221433 ** get_address_of_restartMessageDisplay_10() { return &___restartMessageDisplay_10; }
	inline void set_restartMessageDisplay_10(Text_t356221433 * value)
	{
		___restartMessageDisplay_10 = value;
		Il2CppCodeGenWriteBarrier(&___restartMessageDisplay_10, value);
	}

	inline static int32_t get_offset_of_gameOverScoreOutline_11() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___gameOverScoreOutline_11)); }
	inline GameObject_t1756533147 * get_gameOverScoreOutline_11() const { return ___gameOverScoreOutline_11; }
	inline GameObject_t1756533147 ** get_address_of_gameOverScoreOutline_11() { return &___gameOverScoreOutline_11; }
	inline void set_gameOverScoreOutline_11(GameObject_t1756533147 * value)
	{
		___gameOverScoreOutline_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverScoreOutline_11, value);
	}

	inline static int32_t get_offset_of_musicAudioSource_12() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___musicAudioSource_12)); }
	inline AudioSource_t1135106623 * get_musicAudioSource_12() const { return ___musicAudioSource_12; }
	inline AudioSource_t1135106623 ** get_address_of_musicAudioSource_12() { return &___musicAudioSource_12; }
	inline void set_musicAudioSource_12(AudioSource_t1135106623 * value)
	{
		___musicAudioSource_12 = value;
		Il2CppCodeGenWriteBarrier(&___musicAudioSource_12, value);
	}

	inline static int32_t get_offset_of_gameIsOver_13() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___gameIsOver_13)); }
	inline bool get_gameIsOver_13() const { return ___gameIsOver_13; }
	inline bool* get_address_of_gameIsOver_13() { return &___gameIsOver_13; }
	inline void set_gameIsOver_13(bool value)
	{
		___gameIsOver_13 = value;
	}

	inline static int32_t get_offset_of_playAgainButtons_14() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___playAgainButtons_14)); }
	inline GameObject_t1756533147 * get_playAgainButtons_14() const { return ___playAgainButtons_14; }
	inline GameObject_t1756533147 ** get_address_of_playAgainButtons_14() { return &___playAgainButtons_14; }
	inline void set_playAgainButtons_14(GameObject_t1756533147 * value)
	{
		___playAgainButtons_14 = value;
		Il2CppCodeGenWriteBarrier(&___playAgainButtons_14, value);
	}

	inline static int32_t get_offset_of_playAgainLevelToLoad_15() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___playAgainLevelToLoad_15)); }
	inline String_t* get_playAgainLevelToLoad_15() const { return ___playAgainLevelToLoad_15; }
	inline String_t** get_address_of_playAgainLevelToLoad_15() { return &___playAgainLevelToLoad_15; }
	inline void set_playAgainLevelToLoad_15(String_t* value)
	{
		___playAgainLevelToLoad_15 = value;
		Il2CppCodeGenWriteBarrier(&___playAgainLevelToLoad_15, value);
	}

	inline static int32_t get_offset_of_nextLevelButtons_16() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___nextLevelButtons_16)); }
	inline GameObject_t1756533147 * get_nextLevelButtons_16() const { return ___nextLevelButtons_16; }
	inline GameObject_t1756533147 ** get_address_of_nextLevelButtons_16() { return &___nextLevelButtons_16; }
	inline void set_nextLevelButtons_16(GameObject_t1756533147 * value)
	{
		___nextLevelButtons_16 = value;
		Il2CppCodeGenWriteBarrier(&___nextLevelButtons_16, value);
	}

	inline static int32_t get_offset_of_nextLevelToLoad_17() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___nextLevelToLoad_17)); }
	inline String_t* get_nextLevelToLoad_17() const { return ___nextLevelToLoad_17; }
	inline String_t** get_address_of_nextLevelToLoad_17() { return &___nextLevelToLoad_17; }
	inline void set_nextLevelToLoad_17(String_t* value)
	{
		___nextLevelToLoad_17 = value;
		Il2CppCodeGenWriteBarrier(&___nextLevelToLoad_17, value);
	}

	inline static int32_t get_offset_of_previousLevelToLoad_18() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___previousLevelToLoad_18)); }
	inline String_t* get_previousLevelToLoad_18() const { return ___previousLevelToLoad_18; }
	inline String_t** get_address_of_previousLevelToLoad_18() { return &___previousLevelToLoad_18; }
	inline void set_previousLevelToLoad_18(String_t* value)
	{
		___previousLevelToLoad_18 = value;
		Il2CppCodeGenWriteBarrier(&___previousLevelToLoad_18, value);
	}

	inline static int32_t get_offset_of_gameStartCanvas_19() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___gameStartCanvas_19)); }
	inline GameObject_t1756533147 * get_gameStartCanvas_19() const { return ___gameStartCanvas_19; }
	inline GameObject_t1756533147 ** get_address_of_gameStartCanvas_19() { return &___gameStartCanvas_19; }
	inline void set_gameStartCanvas_19(GameObject_t1756533147 * value)
	{
		___gameStartCanvas_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameStartCanvas_19, value);
	}

	inline static int32_t get_offset_of_mainCanvas_20() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___mainCanvas_20)); }
	inline GameObject_t1756533147 * get_mainCanvas_20() const { return ___mainCanvas_20; }
	inline GameObject_t1756533147 ** get_address_of_mainCanvas_20() { return &___mainCanvas_20; }
	inline void set_mainCanvas_20(GameObject_t1756533147 * value)
	{
		___mainCanvas_20 = value;
		Il2CppCodeGenWriteBarrier(&___mainCanvas_20, value);
	}

	inline static int32_t get_offset_of_currentTime_21() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___currentTime_21)); }
	inline float get_currentTime_21() const { return ___currentTime_21; }
	inline float* get_address_of_currentTime_21() { return &___currentTime_21; }
	inline void set_currentTime_21(float value)
	{
		___currentTime_21 = value;
	}

	inline static int32_t get_offset_of_U3ChasGameStartedU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___U3ChasGameStartedU3Ek__BackingField_22)); }
	inline bool get_U3ChasGameStartedU3Ek__BackingField_22() const { return ___U3ChasGameStartedU3Ek__BackingField_22; }
	inline bool* get_address_of_U3ChasGameStartedU3Ek__BackingField_22() { return &___U3ChasGameStartedU3Ek__BackingField_22; }
	inline void set_U3ChasGameStartedU3Ek__BackingField_22(bool value)
	{
		___U3ChasGameStartedU3Ek__BackingField_22 = value;
	}
};

struct GameManager_t2252321495_StaticFields
{
public:
	// GameManager GameManager::gm
	GameManager_t2252321495 * ___gm_2;

public:
	inline static int32_t get_offset_of_gm_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ___gm_2)); }
	inline GameManager_t2252321495 * get_gm_2() const { return ___gm_2; }
	inline GameManager_t2252321495 ** get_address_of_gm_2() { return &___gm_2; }
	inline void set_gm_2(GameManager_t2252321495 * value)
	{
		___gm_2 = value;
		Il2CppCodeGenWriteBarrier(&___gm_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
