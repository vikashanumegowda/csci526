﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetExit
struct TargetExit_t260853819;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetExit::.ctor()
extern "C"  void TargetExit__ctor_m3373477060 (TargetExit_t260853819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetExit::Start()
extern "C"  void TargetExit_Start_m3657729864 (TargetExit_t260853819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetExit::Update()
extern "C"  void TargetExit_Update_m4167261435 (TargetExit_t260853819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetExit::KillTarget()
extern "C"  void TargetExit_KillTarget_m1027420533 (TargetExit_t260853819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
