﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnGameObjects
struct SpawnGameObjects_t4257146793;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnGameObjects::.ctor()
extern "C"  void SpawnGameObjects__ctor_m1361483562 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnGameObjects::Start()
extern "C"  void SpawnGameObjects_Start_m3096595210 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnGameObjects::Update()
extern "C"  void SpawnGameObjects_Update_m4081916777 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnGameObjects::MakeThingToSpawn()
extern "C"  void SpawnGameObjects_MakeThingToSpawn_m3300773718 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
