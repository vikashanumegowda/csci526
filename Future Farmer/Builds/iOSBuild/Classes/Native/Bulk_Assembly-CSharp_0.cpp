﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BasicController
struct BasicController_t2369989902;
// BasicTargetMover
struct BasicTargetMover_t538935388;
// Controller
struct Controller_t1937198888;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// System.Object
struct Il2CppObject;
// GameManager
struct GameManager_t2252321495;
// JoystickLooker
struct JoystickLooker_t1951869554;
// MouseLooker
struct MouseLooker_t3832072371;
// NextLevel
struct NextLevel_t2321596339;
// UnityEngine.Collision
struct Collision_t2876846408;
// PlayAgain
struct PlayAgain_t1655768986;
// Rotate
struct Rotate_t4255939431;
// Shooter
struct Shooter_t3051933708;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// SpawnGameObjects
struct SpawnGameObjects_t4257146793;
// TargetBehavior
struct TargetBehavior_t2665633703;
// TargetExit
struct TargetExit_t260853819;
// UnityEngine.Animator
struct Animator_t69676727;
// TargetMover
struct TargetMover_t688832618;
// TimedObjectDestructor
struct TimedObjectDestructor_t3793803729;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_BasicController2369989902.h"
#include "AssemblyU2DCSharp_BasicController2369989902MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BasicTargetMover538935388.h"
#include "AssemblyU2DCSharp_BasicTargetMover538935388MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "AssemblyU2DCSharp_Controller1937198888MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_GameManager2252321495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_JoystickLooker1951869554.h"
#include "AssemblyU2DCSharp_JoystickLooker1951869554MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1746754562MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_MouseLooker3832072371.h"
#include "AssemblyU2DCSharp_MouseLooker3832072371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor873194084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096.h"
#include "AssemblyU2DCSharp_NextLevel2321596339.h"
#include "AssemblyU2DCSharp_NextLevel2321596339MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collision2876846408MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayAgain1655768986.h"
#include "AssemblyU2DCSharp_PlayAgain1655768986MethodDeclarations.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_Rotate4255939431MethodDeclarations.h"
#include "AssemblyU2DCSharp_Rotate_whichWayToRotate3838633342.h"
#include "AssemblyU2DCSharp_Rotate_whichWayToRotate3838633342MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shooter3051933708.h"
#include "AssemblyU2DCSharp_Shooter3051933708MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "AssemblyU2DCSharp_SpawnGameObjects4257146793.h"
#include "AssemblyU2DCSharp_SpawnGameObjects4257146793MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_TargetBehavior2665633703.h"
#include "AssemblyU2DCSharp_TargetBehavior2665633703MethodDeclarations.h"
#include "AssemblyU2DCSharp_TargetExit260853819.h"
#include "AssemblyU2DCSharp_TargetExit260853819MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_TargetMover688832618.h"
#include "AssemblyU2DCSharp_TargetMover688832618MethodDeclarations.h"
#include "AssemblyU2DCSharp_TargetMover_motionDirections1066061995.h"
#include "AssemblyU2DCSharp_TargetMover_motionDirections1066061995MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimedObjectDestructor3793803729.h"
#include "AssemblyU2DCSharp_TimedObjectDestructor3793803729MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CharacterController>()
#define GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613(__this, method) ((  CharacterController_t4094781467 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GameManager>()
#define GameObject_GetComponent_TisGameManager_t2252321495_m190008436(__this, method) ((  GameManager_t2252321495 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(__this, method) ((  AudioSource_t1135106623 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BasicController::.ctor()
extern "C"  void BasicController__ctor_m3153565989 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BasicController::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2598240005;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern const uint32_t BasicController_Update_m1265671600_MetadataUsageId;
extern "C"  void BasicController_Update_m1265671600 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicController_Update_m1265671600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2598240005, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BasicTargetMover::.ctor()
extern "C"  void BasicTargetMover__ctor_m3458125707 (BasicTargetMover_t538935388 * __this, const MethodInfo* method)
{
	{
		__this->set_doSpin_2((bool)1);
		__this->set_doMotion_3((bool)1);
		__this->set_spinSpeed_4((180.0f));
		__this->set_motionMagnitude_5((0.1f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BasicTargetMover::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BasicTargetMover_Update_m4246744410_MetadataUsageId;
extern "C"  void BasicTargetMover_Update_m4246744410 (BasicTargetMover_t538935388 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicTargetMover_Update_m4246744410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_doSpin_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_spinSpeed_4();
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_Rotate_m1743927093(L_2, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		bool L_8 = __this->get_doMotion_3();
		if (!L_8)
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_13 = cosf(L_12);
		Vector3_t2243707580  L_14 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_motionMagnitude_5();
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_Translate_m3316827744(L_10, L_16, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		__this->set_moveSpeed_2((3.0f));
		__this->set_jumpSpeed_3((10.0f));
		__this->set_gravity_4((9.81f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveDirection_6(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::Start()
extern const MethodInfo* GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var;
extern const uint32_t Controller_Start_m165415507_MetadataUsageId;
extern "C"  void Controller_Start_m165415507 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_Start_m165415507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		CharacterController_t4094781467 * L_1 = GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var);
		__this->set_myController_5(L_1);
		return;
	}
}
// System.Void Controller::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t Controller_Update_m2428618086_MetadataUsageId;
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_Update_m2428618086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterController_t4094781467 * L_0 = __this->get_myController_5();
		NullCheck(L_0);
		bool L_1 = CharacterController_get_isGrounded_m2594228107(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0082;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_3 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2638739322(&L_4, L_2, (0.0f), L_3, /*hidden argument*/NULL);
		__this->set_moveDirection_6(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = __this->get_moveDirection_6();
		NullCheck(L_5);
		Vector3_t2243707580  L_7 = Transform_TransformDirection_m1639585047(L_5, L_6, /*hidden argument*/NULL);
		__this->set_moveDirection_6(L_7);
		Vector3_t2243707580  L_8 = __this->get_moveDirection_6();
		float L_9 = __this->get_moveSpeed_2();
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_moveDirection_6(L_10);
		bool L_11 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		Vector3_t2243707580 * L_12 = __this->get_address_of_moveDirection_6();
		float L_13 = __this->get_jumpSpeed_3();
		L_12->set_y_2(L_13);
	}

IL_0082:
	{
		Vector3_t2243707580 * L_14 = __this->get_address_of_moveDirection_6();
		Vector3_t2243707580 * L_15 = L_14;
		float L_16 = L_15->get_y_2();
		float L_17 = __this->get_gravity_4();
		float L_18 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_15->set_y_2(((float)((float)L_16-(float)((float)((float)L_17*(float)L_18)))));
		CharacterController_t4094781467 * L_19 = __this->get_myController_5();
		Vector3_t2243707580  L_20 = __this->get_moveDirection_6();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		CharacterController_Move_m3456882757(L_19, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		__this->set_startTime_7((5.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameManager::get_hasGameStarted()
extern "C"  bool GameManager_get_hasGameStarted_m1794466706 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3ChasGameStartedU3Ek__BackingField_22();
		return L_0;
	}
}
// System.Void GameManager::set_hasGameStarted(System.Boolean)
extern "C"  void GameManager_set_hasGameStarted_m2949618165 (GameManager_t2252321495 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ChasGameStartedU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Void GameManager::Start()
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameManager_t2252321495_m190008436_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t GameManager_Start_m2655388892_MetadataUsageId;
extern "C"  void GameManager_Start_m2655388892 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Start_m2655388892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameManager_set_hasGameStarted_m2949618165(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_player_3();
		NullCheck(L_0);
		CharacterController_t4094781467 * L_1 = GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var);
		NullCheck(L_1);
		Collider_set_enabled_m3489100454(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_gameStartCanvas_19();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_mainCanvas_20();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_4 = __this->get_musicAudioSource_12();
		NullCheck(L_4);
		AudioSource_Stop_m3452679614(L_4, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_7();
		__this->set_currentTime_21(L_5);
		GameManager_t2252321495 * L_6 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0067;
		}
	}
	{
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameManager_t2252321495 * L_9 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_8, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_MethodInfo_var);
		((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->set_gm_2(L_9);
	}

IL_0067:
	{
		Text_t356221433 * L_10 = __this->get_mainScoreDisplay_8();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, _stringLiteral372029326);
		GameObject_t1756533147 * L_11 = __this->get_gameOverScoreOutline_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0093;
		}
	}
	{
		GameObject_t1756533147 * L_13 = __this->get_gameOverScoreOutline_11();
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)0, /*hidden argument*/NULL);
	}

IL_0093:
	{
		GameObject_t1756533147 * L_14 = __this->get_playAgainButtons_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00af;
		}
	}
	{
		GameObject_t1756533147 * L_16 = __this->get_playAgainButtons_14();
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		Text_t356221433 * L_17 = __this->get_restartMessageDisplay_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00cf;
		}
	}
	{
		Text_t356221433 * L_19 = __this->get_restartMessageDisplay_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_20);
	}

IL_00cf:
	{
		GameObject_t1756533147 * L_21 = __this->get_nextLevelButtons_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00eb;
		}
	}
	{
		GameObject_t1756533147 * L_23 = __this->get_nextLevelButtons_16();
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)0, /*hidden argument*/NULL);
	}

IL_00eb:
	{
		return;
	}
}
// System.Void GameManager::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2562712940;
extern const uint32_t GameManager_Update_m969954595_MetadataUsageId;
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Update_m969954595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = GameManager_get_hasGameStarted_m1794466706(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = __this->get_gameIsOver_13();
		if (L_1)
		{
			goto IL_00b3;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_player_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_y_2();
		if ((!(((float)L_5) < ((float)(-1.0f)))))
		{
			goto IL_003f;
		}
	}
	{
		GameManager_EndGame_m2742763215(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		bool L_6 = __this->get_canBeatLevel_5();
		if (!L_6)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_7 = __this->get_score_4();
		int32_t L_8 = __this->get_beatLevelScore_6();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0066;
		}
	}
	{
		GameManager_BeatLevel_m1637775698(__this, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_0066:
	{
		float L_9 = __this->get_currentTime_21();
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		GameManager_EndGame_m2742763215(__this, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_0081:
	{
		float L_10 = __this->get_currentTime_21();
		float L_11 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentTime_21(((float)((float)L_10-(float)L_11)));
		Text_t356221433 * L_12 = __this->get_mainTimerDisplay_9();
		float* L_13 = __this->get_address_of_currentTime_21();
		String_t* L_14 = Single_ToString_m2359963436(L_13, _stringLiteral2562712940, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_14);
	}

IL_00ae:
	{
		goto IL_00dc;
	}

IL_00b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)114), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ca;
		}
	}
	{
		GameManager_RestartGame_m1828089451(__this, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_16 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)112), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00dc;
		}
	}
	{
		GameManager_LoadPreviousLevel_m569945163(__this, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void GameManager::EndGame()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1812935746;
extern Il2CppCodeGenString* _stringLiteral4197105902;
extern const uint32_t GameManager_EndGame_m2742763215_MetadataUsageId;
extern "C"  void GameManager_EndGame_m2742763215 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_EndGame_m2742763215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameIsOver_13((bool)1);
		Text_t356221433 * L_0 = __this->get_mainTimerDisplay_9();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral1812935746);
		GameObject_t1756533147 * L_1 = __this->get_gameOverScoreOutline_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_gameOverScoreOutline_11();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0033:
	{
		Text_t356221433 * L_4 = __this->get_restartMessageDisplay_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0053;
		}
	}
	{
		Text_t356221433 * L_6 = __this->get_restartMessageDisplay_10();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral4197105902);
	}

IL_0053:
	{
		GameObject_t1756533147 * L_7 = __this->get_playAgainButtons_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get_playAgainButtons_14();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_006f:
	{
		AudioSource_t1135106623 * L_10 = __this->get_musicAudioSource_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008f;
		}
	}
	{
		AudioSource_t1135106623 * L_12 = __this->get_musicAudioSource_12();
		NullCheck(L_12);
		AudioSource_set_pitch_m3064416458(L_12, (0.5f), /*hidden argument*/NULL);
	}

IL_008f:
	{
		GameObject_t1756533147 * L_13 = __this->get_player_3();
		NullCheck(L_13);
		CharacterController_t4094781467 * L_14 = GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613(L_13, /*hidden argument*/GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var);
		NullCheck(L_14);
		Collider_set_enabled_m3489100454(L_14, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::BeatLevel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral609772327;
extern Il2CppCodeGenString* _stringLiteral4197105902;
extern const uint32_t GameManager_BeatLevel_m1637775698_MetadataUsageId;
extern "C"  void GameManager_BeatLevel_m1637775698 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_BeatLevel_m1637775698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameIsOver_13((bool)1);
		Text_t356221433 * L_0 = __this->get_mainTimerDisplay_9();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral609772327);
		GameObject_t1756533147 * L_1 = __this->get_gameOverScoreOutline_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_gameOverScoreOutline_11();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0033:
	{
		Text_t356221433 * L_4 = __this->get_restartMessageDisplay_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0053;
		}
	}
	{
		Text_t356221433 * L_6 = __this->get_restartMessageDisplay_10();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral4197105902);
	}

IL_0053:
	{
		GameObject_t1756533147 * L_7 = __this->get_nextLevelButtons_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get_nextLevelButtons_16();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_006f:
	{
		AudioSource_t1135106623 * L_10 = __this->get_musicAudioSource_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008f;
		}
	}
	{
		AudioSource_t1135106623 * L_12 = __this->get_musicAudioSource_12();
		NullCheck(L_12);
		AudioSource_set_pitch_m3064416458(L_12, (0.5f), /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void GameManager::targetHit(System.Int32,System.Single)
extern Il2CppCodeGenString* _stringLiteral2562712940;
extern const uint32_t GameManager_targetHit_m589975860_MetadataUsageId;
extern "C"  void GameManager_targetHit_m589975860 (GameManager_t2252321495 * __this, int32_t ___scoreAmount0, float ___timeAmount1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_targetHit_m589975860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_score_4();
		int32_t L_1 = ___scoreAmount0;
		__this->set_score_4(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		Text_t356221433 * L_2 = __this->get_mainScoreDisplay_8();
		int32_t* L_3 = __this->get_address_of_score_4();
		String_t* L_4 = Int32_ToString_m2960866144(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		float L_5 = __this->get_currentTime_21();
		float L_6 = ___timeAmount1;
		__this->set_currentTime_21(((float)((float)L_5+(float)L_6)));
		float L_7 = __this->get_currentTime_21();
		if ((!(((float)L_7) < ((float)(0.0f)))))
		{
			goto IL_0053;
		}
	}
	{
		__this->set_currentTime_21((0.0f));
	}

IL_0053:
	{
		Text_t356221433 * L_8 = __this->get_mainTimerDisplay_9();
		float* L_9 = __this->get_address_of_currentTime_21();
		String_t* L_10 = Single_ToString_m2359963436(L_9, _stringLiteral2562712940, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_10);
		return;
	}
}
// System.Void GameManager::RestartGame()
extern "C"  void GameManager_RestartGame_m1828089451 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_playAgainLevelToLoad_15();
		Application_LoadLevel_m393995325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::NextLevel()
extern "C"  void GameManager_NextLevel_m1326141459 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_nextLevelToLoad_17();
		Application_LoadLevel_m393995325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::StartGame()
extern const MethodInfo* GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var;
extern const uint32_t GameManager_StartGame_m4019248290_MetadataUsageId;
extern "C"  void GameManager_StartGame_m4019248290 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_StartGame_m4019248290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_player_3();
		NullCheck(L_0);
		CharacterController_t4094781467 * L_1 = GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacterController_t4094781467_m1462576613_MethodInfo_var);
		NullCheck(L_1);
		Collider_set_enabled_m3489100454(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_gameStartCanvas_19();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_mainCanvas_20();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		GameManager_set_hasGameStarted_m2949618165(__this, (bool)1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_4 = __this->get_musicAudioSource_12();
		NullCheck(L_4);
		AudioSource_Play_m353744792(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::LoadPreviousLevel()
extern "C"  void GameManager_LoadPreviousLevel_m569945163 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_previousLevelToLoad_18();
		Application_LoadLevel_m393995325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoystickLooker::.ctor()
extern "C"  void JoystickLooker__ctor_m1119335649 (JoystickLooker_t1951869554 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoystickLooker::Start()
extern Il2CppClass* EventSystem_t3466835263_il2cpp_TypeInfo_var;
extern const uint32_t JoystickLooker_Start_m1378765489_MetadataUsageId;
extern "C"  void JoystickLooker_Start_m1378765489 (JoystickLooker_t1951869554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoystickLooker_Start_m1378765489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		__this->set_character_4(L_1);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		__this->set_cameraTransform_5(L_3);
		Transform_t3275118058 * L_4 = __this->get_character_4();
		NullCheck(L_4);
		Quaternion_t4030073918  L_5 = Transform_get_localRotation_m4001487205(L_4, /*hidden argument*/NULL);
		__this->set_m_CharacterTargetRot_2(L_5);
		Transform_t3275118058 * L_6 = __this->get_cameraTransform_5();
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_localRotation_m4001487205(L_6, /*hidden argument*/NULL);
		__this->set_m_CameraTargetRot_3(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t3466835263_il2cpp_TypeInfo_var);
		EventSystem_t3466835263 * L_8 = EventSystem_get_current_m319019811(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_m_joystick_6();
		NullCheck(L_8);
		EventSystem_SetSelectedGameObject_m2232036508(L_8, L_9, (BaseEventData_t2681005625 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoystickLooker::Update()
extern Il2CppClass* CrossPlatformInputManager_t1746754562_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029420;
extern Il2CppCodeGenString* _stringLiteral372029414;
extern const uint32_t JoystickLooker_Update_m2970283440_MetadataUsageId;
extern "C"  void JoystickLooker_Update_m2970283440 (JoystickLooker_t1951869554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoystickLooker_Update_m2970283440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t1746754562_il2cpp_TypeInfo_var);
		float L_0 = CrossPlatformInputManager_GetAxis_m128371830(NULL /*static, unused*/, _stringLiteral372029420, /*hidden argument*/NULL);
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_3 = CrossPlatformInputManager_GetAxis_m128371830(NULL /*static, unused*/, _stringLiteral372029414, /*hidden argument*/NULL);
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		JoystickLooker_LookRotation_m898385688(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoystickLooker::LookRotation()
extern Il2CppClass* CrossPlatformInputManager_t1746754562_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern const uint32_t JoystickLooker_LookRotation_m898385688_MetadataUsageId;
extern "C"  void JoystickLooker_LookRotation_m898385688 (JoystickLooker_t1951869554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoystickLooker_LookRotation_m898385688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t1746754562_il2cpp_TypeInfo_var);
		float L_0 = CrossPlatformInputManager_GetAxis_m128371830(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = CrossPlatformInputManager_GetAxis_m128371830(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		V_1 = L_1;
		Quaternion_t4030073918  L_2 = __this->get_m_CharacterTargetRot_2();
		float L_3 = V_0;
		Quaternion_t4030073918  L_4 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), L_3, (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		__this->set_m_CharacterTargetRot_2(L_5);
		Quaternion_t4030073918  L_6 = __this->get_m_CameraTargetRot_3();
		float L_7 = V_1;
		Quaternion_t4030073918  L_8 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, ((-L_7)), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_9 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		__this->set_m_CameraTargetRot_3(L_9);
		Transform_t3275118058 * L_10 = __this->get_character_4();
		Quaternion_t4030073918  L_11 = __this->get_m_CharacterTargetRot_2();
		NullCheck(L_10);
		Transform_set_localRotation_m2055111962(L_10, L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = __this->get_cameraTransform_5();
		Quaternion_t4030073918  L_13 = __this->get_m_CameraTargetRot_3();
		NullCheck(L_12);
		Transform_set_localRotation_m2055111962(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLooker::.ctor()
extern "C"  void MouseLooker__ctor_m1372322462 (MouseLooker_t3832072371 * __this, const MethodInfo* method)
{
	{
		__this->set_XSensitivity_2((2.0f));
		__this->set_YSensitivity_3((2.0f));
		__this->set_clampVerticalRotation_4((bool)1);
		__this->set_MinimumX_5((-90.0f));
		__this->set_MaximumX_6((90.0f));
		__this->set_smoothTime_8((5.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLooker::Start()
extern "C"  void MouseLooker_Start_m4216777146 (MouseLooker_t3832072371 * __this, const MethodInfo* method)
{
	{
		MouseLooker_LockCursor_m886230828(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		__this->set_character_11(L_1);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		__this->set_cameraTransform_12(L_3);
		Transform_t3275118058 * L_4 = __this->get_character_11();
		NullCheck(L_4);
		Quaternion_t4030073918  L_5 = Transform_get_localRotation_m4001487205(L_4, /*hidden argument*/NULL);
		__this->set_m_CharacterTargetRot_9(L_5);
		Transform_t3275118058 * L_6 = __this->get_cameraTransform_12();
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_localRotation_m4001487205(L_6, /*hidden argument*/NULL);
		__this->set_m_CameraTargetRot_10(L_7);
		return;
	}
}
// System.Void MouseLooker::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2358390244;
extern Il2CppCodeGenString* _stringLiteral3645101709;
extern const uint32_t MouseLooker_Update_m3524100883_MetadataUsageId;
extern "C"  void MouseLooker_Update_m3524100883 (MouseLooker_t3832072371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLooker_Update_m3524100883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MouseLooker_LookRotation_m3803574813(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral2358390244, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		MouseLooker_LockCursor_m886230828(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		MouseLooker_LockCursor_m886230828(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void MouseLooker::LockCursor(System.Boolean)
extern "C"  void MouseLooker_LockCursor_m886230828 (MouseLooker_t3832072371 * __this, bool ___isLocked0, const MethodInfo* method)
{
	{
		bool L_0 = ___isLocked0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Cursor_set_lockState_m387168319(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_0017:
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Cursor_set_lockState_m387168319(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void MouseLooker::LookRotation()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern const uint32_t MouseLooker_LookRotation_m3803574813_MetadataUsageId;
extern "C"  void MouseLooker_LookRotation_m3803574813 (MouseLooker_t3832072371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLooker_LookRotation_m3803574813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_1 = __this->get_XSensitivity_2();
		V_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_3 = __this->get_YSensitivity_3();
		V_1 = ((float)((float)L_2*(float)L_3));
		Quaternion_t4030073918  L_4 = __this->get_m_CharacterTargetRot_9();
		float L_5 = V_0;
		Quaternion_t4030073918  L_6 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), L_5, (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		__this->set_m_CharacterTargetRot_9(L_7);
		Quaternion_t4030073918  L_8 = __this->get_m_CameraTargetRot_10();
		float L_9 = V_1;
		Quaternion_t4030073918  L_10 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, ((-L_9)), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_11 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		__this->set_m_CameraTargetRot_10(L_11);
		bool L_12 = __this->get_clampVerticalRotation_4();
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		Quaternion_t4030073918  L_13 = __this->get_m_CameraTargetRot_10();
		Quaternion_t4030073918  L_14 = MouseLooker_ClampRotationAroundXAxis_m180921578(__this, L_13, /*hidden argument*/NULL);
		__this->set_m_CameraTargetRot_10(L_14);
	}

IL_0084:
	{
		bool L_15 = __this->get_smooth_7();
		if (!L_15)
		{
			goto IL_00ee;
		}
	}
	{
		Transform_t3275118058 * L_16 = __this->get_character_11();
		Transform_t3275118058 * L_17 = __this->get_character_11();
		NullCheck(L_17);
		Quaternion_t4030073918  L_18 = Transform_get_localRotation_m4001487205(L_17, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_19 = __this->get_m_CharacterTargetRot_9();
		float L_20 = __this->get_smoothTime_8();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_22 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_18, L_19, ((float)((float)L_20*(float)L_21)), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localRotation_m2055111962(L_16, L_22, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = __this->get_cameraTransform_12();
		Transform_t3275118058 * L_24 = __this->get_cameraTransform_12();
		NullCheck(L_24);
		Quaternion_t4030073918  L_25 = Transform_get_localRotation_m4001487205(L_24, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_26 = __this->get_m_CameraTargetRot_10();
		float L_27 = __this->get_smoothTime_8();
		float L_28 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_29 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_25, L_26, ((float)((float)L_27*(float)L_28)), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_localRotation_m2055111962(L_23, L_29, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_00ee:
	{
		Transform_t3275118058 * L_30 = __this->get_character_11();
		Quaternion_t4030073918  L_31 = __this->get_m_CharacterTargetRot_9();
		NullCheck(L_30);
		Transform_set_localRotation_m2055111962(L_30, L_31, /*hidden argument*/NULL);
		Transform_t3275118058 * L_32 = __this->get_cameraTransform_12();
		Quaternion_t4030073918  L_33 = __this->get_m_CameraTargetRot_10();
		NullCheck(L_32);
		Transform_set_localRotation_m2055111962(L_32, L_33, /*hidden argument*/NULL);
	}

IL_0110:
	{
		return;
	}
}
// UnityEngine.Quaternion MouseLooker::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t MouseLooker_ClampRotationAroundXAxis_m180921578_MetadataUsageId;
extern "C"  Quaternion_t4030073918  MouseLooker_ClampRotationAroundXAxis_m180921578 (MouseLooker_t3832072371 * __this, Quaternion_t4030073918  ___q0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLooker_ClampRotationAroundXAxis_m180921578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t4030073918 * L_0 = (&___q0);
		float L_1 = L_0->get_x_0();
		float L_2 = (&___q0)->get_w_3();
		L_0->set_x_0(((float)((float)L_1/(float)L_2)));
		Quaternion_t4030073918 * L_3 = (&___q0);
		float L_4 = L_3->get_y_1();
		float L_5 = (&___q0)->get_w_3();
		L_3->set_y_1(((float)((float)L_4/(float)L_5)));
		Quaternion_t4030073918 * L_6 = (&___q0);
		float L_7 = L_6->get_z_2();
		float L_8 = (&___q0)->get_w_3();
		L_6->set_z_2(((float)((float)L_7/(float)L_8)));
		(&___q0)->set_w_3((1.0f));
		float L_9 = (&___q0)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = atanf(L_9);
		V_0 = ((float)((float)(114.59156f)*(float)L_10));
		float L_11 = V_0;
		float L_12 = __this->get_MinimumX_5();
		float L_13 = __this->get_MaximumX_6();
		float L_14 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		float L_15 = V_0;
		float L_16 = tanf(((float)((float)(0.008726646f)*(float)L_15)));
		(&___q0)->set_x_0(L_16);
		Quaternion_t4030073918  L_17 = ___q0;
		return L_17;
	}
}
// System.Void NextLevel::.ctor()
extern "C"  void NextLevel__ctor_m2680661740 (NextLevel_t2321596339 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NextLevel::OnCollisionEnter(UnityEngine.Collision)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral469617985;
extern const uint32_t NextLevel_OnCollisionEnter_m1872425830_MetadataUsageId;
extern "C"  void NextLevel_OnCollisionEnter_m1872425830 (NextLevel_t2321596339 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NextLevel_OnCollisionEnter_m1872425830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision_t2876846408 * L_0 = ___newCollision0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision_get_gameObject_m1370363400(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral469617985, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameManager_t2252321495 * L_4 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_4);
		GameManager_NextLevel_m1326141459(L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void PlayAgain::.ctor()
extern "C"  void PlayAgain__ctor_m1254445871 (PlayAgain_t1655768986 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayAgain::OnCollisionEnter(UnityEngine.Collision)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral469617985;
extern const uint32_t PlayAgain_OnCollisionEnter_m3365844533_MetadataUsageId;
extern "C"  void PlayAgain_OnCollisionEnter_m3365844533 (PlayAgain_t1655768986 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayAgain_OnCollisionEnter_m3365844533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision_t2876846408 * L_0 = ___newCollision0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision_get_gameObject_m1370363400(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral469617985, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameManager_t2252321495 * L_4 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_4);
		GameManager_RestartGame_m1828089451(L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1487335446 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_2((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Update()
extern "C"  void Rotate_Update_m4217433871 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_way_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0074;
		}
	}
	{
		goto IL_009e;
	}

IL_0020:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_speed_2();
		Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Rotate_m1743927093(L_4, L_9, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_004a:
	{
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = __this->get_speed_2();
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_Rotate_m1743927093(L_10, L_15, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_0074:
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		float L_20 = __this->get_speed_2();
		Vector3_t2243707580  L_21 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_Rotate_m1743927093(L_16, L_21, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_009e:
	{
		return;
	}
}
// System.Void Shooter::.ctor()
extern "C"  void Shooter__ctor_m2111854087 (Shooter_t3051933708 * __this, const MethodInfo* method)
{
	{
		__this->set_power_3((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shooter::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3645101709;
extern const uint32_t Shooter_Update_m3198244586_MetadataUsageId;
extern "C"  void Shooter_Update_m3198244586 (Shooter_t3051933708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shooter_Update_m3198244586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00ef;
		}
	}
	{
		GameManager_t2252321495 * L_1 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_1);
		bool L_2 = GameManager_get_hasGameStarted_m1794466706(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		GameManager_t2252321495 * L_3 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_3);
		GameManager_StartGame_m4019248290(L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		GameObject_t1756533147 * L_4 = __this->get_projectile_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00ef;
		}
	}
	{
		GameObject_t1756533147 * L_6 = __this->get_projectile_2();
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_forward_m1833488937(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Quaternion_t4030073918  L_13 = Transform_get_rotation_m1033555130(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_14 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_6, L_11, L_13, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_14;
		GameObject_t1756533147 * L_15 = V_0;
		NullCheck(L_15);
		Rigidbody_t4233889191 * L_16 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_15, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0081;
		}
	}
	{
		GameObject_t1756533147 * L_18 = V_0;
		NullCheck(L_18);
		GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_18, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
	}

IL_0081:
	{
		GameObject_t1756533147 * L_19 = V_0;
		NullCheck(L_19);
		Rigidbody_t4233889191 * L_20 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_19, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_forward_m1833488937(L_21, /*hidden argument*/NULL);
		float L_23 = __this->get_power_3();
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Rigidbody_AddForce_m3219459786(L_20, L_24, 2, /*hidden argument*/NULL);
		AudioClip_t1932558630 * L_25 = __this->get_shootSFX_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ef;
		}
	}
	{
		GameObject_t1756533147 * L_27 = V_0;
		NullCheck(L_27);
		AudioSource_t1135106623 * L_28 = GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(L_27, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00d9;
		}
	}
	{
		GameObject_t1756533147 * L_30 = V_0;
		NullCheck(L_30);
		AudioSource_t1135106623 * L_31 = GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(L_30, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var);
		AudioClip_t1932558630 * L_32 = __this->get_shootSFX_4();
		NullCheck(L_31);
		AudioSource_PlayOneShot_m286472761(L_31, L_32, /*hidden argument*/NULL);
		goto IL_00ef;
	}

IL_00d9:
	{
		AudioClip_t1932558630 * L_33 = __this->get_shootSFX_4();
		GameObject_t1756533147 * L_34 = V_0;
		NullCheck(L_34);
		Transform_t3275118058 * L_35 = GameObject_get_transform_m909382139(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t2243707580  L_36 = Transform_get_position_m1104419803(L_35, /*hidden argument*/NULL);
		AudioSource_PlayClipAtPoint_m1513558507(NULL /*static, unused*/, L_33, L_36, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		return;
	}
}
// System.Void SpawnGameObjects::.ctor()
extern "C"  void SpawnGameObjects__ctor_m1361483562 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method)
{
	{
		__this->set_secondsBetweenSpawning_2((0.1f));
		__this->set_xMinRange_3((-25.0f));
		__this->set_xMaxRange_4((25.0f));
		__this->set_yMinRange_5((8.0f));
		__this->set_yMaxRange_6((25.0f));
		__this->set_zMinRange_7((-25.0f));
		__this->set_zMaxRange_8((25.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnGameObjects::Start()
extern "C"  void SpawnGameObjects_Start_m3096595210 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_secondsBetweenSpawning_2();
		__this->set_nextSpawnTime_10(((float)((float)L_0+(float)L_1)));
		return;
	}
}
// System.Void SpawnGameObjects::Update()
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3967588682;
extern const uint32_t SpawnGameObjects_Update_m4081916777_MetadataUsageId;
extern "C"  void SpawnGameObjects_Update_m4081916777 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnGameObjects_Update_m4081916777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameManager_t2252321495 * L_0 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		GameManager_t2252321495 * L_2 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_gameIsOver_13();
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		float L_4 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_nextSpawnTime_10();
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3967588682, /*hidden argument*/NULL);
		SpawnGameObjects_MakeThingToSpawn_m3300773718(__this, /*hidden argument*/NULL);
		float L_6 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_secondsBetweenSpawning_2();
		__this->set_nextSpawnTime_10(((float)((float)L_6+(float)L_7)));
	}

IL_0051:
	{
		return;
	}
}
// System.Void SpawnGameObjects::MakeThingToSpawn()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t SpawnGameObjects_MakeThingToSpawn_m3300773718_MetadataUsageId;
extern "C"  void SpawnGameObjects_MakeThingToSpawn_m3300773718 (SpawnGameObjects_t4257146793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnGameObjects_MakeThingToSpawn_m3300773718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	GameObject_t1756533147 * V_2 = NULL;
	{
		float L_0 = __this->get_xMinRange_3();
		float L_1 = __this->get_xMaxRange_4();
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_2);
		float L_3 = __this->get_yMinRange_5();
		float L_4 = __this->get_yMaxRange_6();
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		(&V_0)->set_y_2(L_5);
		float L_6 = __this->get_zMinRange_7();
		float L_7 = __this->get_zMaxRange_8();
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		(&V_0)->set_z_3(L_8);
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_spawnObjects_9();
		NullCheck(L_9);
		int32_t L_10 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_10;
		GameObjectU5BU5D_t3057952154* L_11 = __this->get_spawnObjects_9();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		GameObject_t1756533147 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		Vector3_t2243707580  L_15 = V_0;
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t4030073918  L_17 = Transform_get_rotation_m1033555130(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_18 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_14, L_15, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_2 = L_18;
		GameObject_t1756533147 * L_19 = V_2;
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = GameObject_get_transform_m909382139(L_19, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = GameObject_get_transform_m909382139(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_parent_m3281327839(L_20, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetBehavior::.ctor()
extern "C"  void TargetBehavior__ctor_m3571505626 (TargetBehavior_t2665633703 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetBehavior::OnCollisionEnter(UnityEngine.Collision)
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral469617985;
extern const uint32_t TargetBehavior_OnCollisionEnter_m860962568_MetadataUsageId;
extern "C"  void TargetBehavior_OnCollisionEnter_m860962568 (TargetBehavior_t2665633703 * __this, Collision_t2876846408 * ___newCollision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TargetBehavior_OnCollisionEnter_m860962568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameManager_t2252321495 * L_0 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		GameManager_t2252321495 * L_2 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_gameIsOver_13();
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Collision_t2876846408 * L_4 = ___newCollision0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Collision_get_gameObject_m1370363400(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m1425941094(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral469617985, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_explosionPrefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		GameObject_t1756533147 * L_10 = __this->get_explosionPrefab_4();
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Quaternion_t4030073918  L_14 = Transform_get_rotation_m1033555130(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_10, L_12, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
	}

IL_006b:
	{
		GameManager_t2252321495 * L_15 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0090;
		}
	}
	{
		GameManager_t2252321495 * L_17 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_gm_2();
		int32_t L_18 = __this->get_scoreAmount_2();
		float L_19 = __this->get_timeAmount_3();
		NullCheck(L_17);
		GameManager_targetHit_m589975860(L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_0090:
	{
		Collision_t2876846408 * L_20 = ___newCollision0;
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Collision_get_gameObject_m1370363400(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		return;
	}
}
// System.Void TargetExit::.ctor()
extern "C"  void TargetExit__ctor_m3373477060 (TargetExit_t260853819 * __this, const MethodInfo* method)
{
	{
		__this->set_exitAfterSeconds_2((10.0f));
		__this->set_exitAnimationSeconds_3((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetExit::Start()
extern "C"  void TargetExit_Start_m3657729864 (TargetExit_t260853819 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_exitAfterSeconds_2();
		__this->set_targetTime_5(((float)((float)L_0+(float)L_1)));
		return;
	}
}
// System.Void TargetExit::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral1126561467;
extern const uint32_t TargetExit_Update_m4167261435_MetadataUsageId;
extern "C"  void TargetExit_Update_m4167261435 (TargetExit_t260853819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TargetExit_Update_m4167261435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_targetTime_5();
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0064;
		}
	}
	{
		Animator_t69676727 * L_2 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_0031:
	{
		bool L_5 = __this->get_startDestroy_4();
		if (L_5)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_startDestroy_4((bool)1);
		Animator_t69676727 * L_6 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		NullCheck(L_6);
		Animator_SetTrigger_m3418492570(L_6, _stringLiteral3558534422, /*hidden argument*/NULL);
		float L_7 = __this->get_exitAnimationSeconds_3();
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1126561467, L_7, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void TargetExit::KillTarget()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TargetExit_KillTarget_m1027420533_MetadataUsageId;
extern "C"  void TargetExit_KillTarget_m1027420533 (TargetExit_t260853819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TargetExit_KillTarget_m1027420533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetMover::.ctor()
extern "C"  void TargetMover__ctor_m1600658069 (TargetMover_t688832618 * __this, const MethodInfo* method)
{
	{
		__this->set_motionState_2(1);
		__this->set_spinSpeed_3((180.0f));
		__this->set_motionMagnitude_4((0.1f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetMover::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TargetMover_Update_m1970153192_MetadataUsageId;
extern "C"  void TargetMover_Update_m1970153192 (TargetMover_t688832618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TargetMover_Update_m1970153192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_motionState_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00b7;
	}

IL_0020:
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_spinSpeed_3();
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		float L_9 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_Rotate_m1743927093(L_5, L_10, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_004f:
	{
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_15 = cosf(L_14);
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		float L_17 = __this->get_motionMagnitude_4();
		Vector3_t2243707580  L_18 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_Translate_m3316827744(L_12, L_18, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_0083:
	{
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = GameObject_get_transform_m909382139(L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_23 = cosf(L_22);
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		float L_25 = __this->get_motionMagnitude_4();
		Vector3_t2243707580  L_26 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_Translate_m3316827744(L_20, L_26, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_00b7:
	{
		return;
	}
}
// System.Void TimedObjectDestructor::.ctor()
extern "C"  void TimedObjectDestructor__ctor_m1281788410 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method)
{
	{
		__this->set_timeOut_2((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedObjectDestructor::Awake()
extern Il2CppCodeGenString* _stringLiteral3022628064;
extern const uint32_t TimedObjectDestructor_Awake_m3826250289_MetadataUsageId;
extern "C"  void TimedObjectDestructor_Awake_m3826250289 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimedObjectDestructor_Awake_m3826250289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_timeOut_2();
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3022628064, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedObjectDestructor::DestroyNow()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TimedObjectDestructor_DestroyNow_m2242008382_MetadataUsageId;
extern "C"  void TimedObjectDestructor_DestroyNow_m2242008382 (TimedObjectDestructor_t3793803729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimedObjectDestructor_DestroyNow_m2242008382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_detachChildren_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_DetachChildren_m2873894053(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
