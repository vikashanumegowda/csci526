﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasicController
struct BasicController_t2369989902;

#include "codegen/il2cpp-codegen.h"

// System.Void BasicController::.ctor()
extern "C"  void BasicController__ctor_m3153565989 (BasicController_t2369989902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasicController::Update()
extern "C"  void BasicController_Update_m1265671600 (BasicController_t2369989902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
